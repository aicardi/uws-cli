[![pipeline status](https://gitlab.obspm.fr/aicardi/uws-cli/badges/master/pipeline.svg)](https://gitlab.obspm.fr/aicardi/uws-cli/commits/master)

# Tools to submit UWS jobs with CLI

## Installation

In a directory of your choice, clone this version of uws-client.

```
git clone https://github.com/aicardi-obspm/uws-client
cd uws-client
git checkout python3-support
pip install .
```

This installs `uws` into your systems `bin` directory and makes the command
available on the command line.

## Shell sample

The file [`scripts/job.sh`](https://gitlab.obspm.fr/aicardi/uws-cli/blob/master/scripts/job.sh)
contains a sample shell script to launch a job, wait for its completion et retrieve its results.

```{.bash include=scripts/job.sh .numberLines}
```

## Pure REST shell sample

If you don't want to install `uws-client`, the file [`scripts/job-rest.sh`](https://gitlab.obspm.fr/aicardi/uws-cli/blob/master/scripts/job-rest.sh) contains a sample shell scripts to lauch a job using only cURL and basic shell commands.

```{.bash include=scripts/job-rest.sh .numberLines}
```

## Python sample

The file [`scripts/job.py`](https://gitlab.obspm.fr/aicardi/uws-cli/blob/master/scripts/job.py) 
contains a sample python script to launch a job, wait for its completion et retrieve its results.

```{.python include=scripts/job.py .numberLines}
```

## PHP REST sample

The file [`scripts/job-rest.php`](https://gitlab.obspm.fr/aicardi/uws-cli/blob/master/scripts/job-rest.php) contains a sample PHP script using the REST interface.

```{.php include=scripts/job-rest.php .numberLines}
```

