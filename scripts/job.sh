#!/bin/bash

## Configure the following variables
SERVER="myserver.mydomain"
JID="jobname"
LOGIN="login"
TOKEN="token"


FILE="$1"

TMPDIR=`mktemp -d`
uws -H https://$SERVER/rest/$JID -U $LOGIN --password $TOKEN job new --run config=@$FILE runId=test_job_uws > $TMPDIR/uws_submission

if grep -q 'An error occurred:' $TMPDIR/uws_submission ; then
	echo "error with job submission"
	cat $TMPDIR/uws_submission
	rm -rf $TMPDIR
	exit 2
fi


JOBID=` grep '^Job ID:' $TMPDIR/uws_submission | cut -f3 -d\ `

echo "Job: $JOBID"

while true; do
	STATUS=`uws -H https://$SERVER/rest/$JID -U $LOGIN --password $TOKEN job phase $JOBID`
	if echo "$STATUS" | grep -q "An error" ; then
		echo "Error with uws"
		echo "$STATUS"
		rm -rf $TMPDIR
		exit 2
	fi
	if [ "$STATUS" == "COMPLETED" ]; then
		echo "Job completed"
		uws -H https://$SERVER/rest/$JID -U $LOGIN --password $TOKEN job results $JOBID
		rm -rf $TMPDIR
		exit 0
	elif [ "$STATUS" == "ERROR" ]; then
		echo "Job completed with error"
		rm -rf $TMPDIR
		exit 1
	fi
	sleep 2
done

