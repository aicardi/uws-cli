#!/usr/bin/env python3

from uws import UWS

## Configure the following variables
SERVER="myserver.mydomain"
JID="jobname"
LOGIN="login"
TOKEN="token"

## Add any parameter to your job here
parameters = {'config':'@myconfigurationfile',
              'runId':'my_job_name'}

## 
uws_client = UWS.client.Client(url=f"https://{SERVER}/rest/{JID}", user=LOGIN, password=TOKEN)


job = uws_client.new_job(parameters)
job = uws_client.run_job(job.job_id)

print(f"Job : {job.job_id}")

while True:
    time.sleep(2)
    phase = uws_client.get_phase(job.job_id)
    if phase == UWS.models.JobPhases.COMPLETED:
        print("Job completed")
        break
    elif phase == UWS.models.JobPhases.ERROR or phase == UWS.models.JobPhases.ABORTED:
        print("Job failed")
        break

job = uws_client.get_job(job.job_id)
for result in job.results:
    filename = "./" + result.rid
    print(f"Downloading {result.rid}")
    uws_client.connection.download_file(str(result.reference), LOGIN, TOKEN, filename)

