#!/bin/bash

## Configure the following variables
SERVER="myserver.mydomain"
JID="jobname"
LOGIN="login"
TOKEN="token"


## Parameter file is passed as first argument
FILE="$1"

TMPDIR=`mktemp -d`

# Creates a job and get its id
jobid=`curl -i -s -X POST --basic -u "$LOGIN:$TOKEN" -F config=@$FILE -F runId=test_job https://$SERVER/rest/$JID/ | grep '^Location' | sed -e 's,.*/,,;s/\r//'`

# Start the job
curl -X POST -s --basic -u "$LOGIN:$TOKEN" -F PHASE=RUN https://$SERVER/rest/$JID/$jobid/phase/

echo "Job: $jobid"

while true; do
	# fetch the job state (which is an xml file) and the uws:phase value
	STATUS=`curl -s --basic -u "$LOGIN:$TOKEN" https://$SERVER/rest/$JID/$jobid/ | sed -ze 's/.*<uws:phase>//;s,</uws:phase>.*$,,'`
	if [ "$STATUS" == "COMPLETED" ]; then
		echo 
		echo "Job completed"
		echo "Results :"
		# fetch the list of results of the job and loop over each uws:result entity
		curl -s --basic -u "$LOGIN:$TOKEN" https://$SERVER/rest/$JID/$jobid/results/ |sed -e 's/>/>\n/g' | grep '<uws:result ' > $TMPDIR/results.xml
		while read -r line ; do 
			# a result looks like <uws:result id="filename" mime-type="something" xlink:href="url" />
			filename=`echo $line | sed -e 's/.*id="//;s/".*$//'`
			url=`echo $line | sed -e 's/.*xlink:href="//;s/".*$//'`
			echo "Get $filename"
			curl -s --basic -u "$LOGIN:$TOKEN" $url -o $filename
		done < $TMPDIR/results.xml
		rm -rf $TMPDIR

		exit 0
	elif [ "$STATUS" == "ERROR" ]; then
		echo 
		echo "Job completed with errors :"
		curl -s --basic -u "$LOGIN:$TOKEN" https://$SERVER/rest/$JID/$jobid/stderr
		exit 1
	fi
	echo -n "."
	sleep 2
done

exit 0
