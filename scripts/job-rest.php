<?php 
## Configure the following variables
$SERVER="myserver.mydomain";
$JID="jobname";
$LOGIN="login";
$TOKEN="token";

## Parameter file is passed as first argument
$FILE=$argv[1];

function post_uws($parameter, $path="") {
  global $SERVER,$JID,$LOGIN,$TOKEN;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, "https://$SERVER/rest/$JID/".$path);
  curl_setopt($curl, CURLOPT_POST, true);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $parameter);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($curl, CURLOPT_USERPWD, $LOGIN . ":" . $TOKEN);

  $return = curl_exec($curl);
  curl_close($curl);
  return $return;
}
function get_uws($path="") {
  global $SERVER,$JID,$LOGIN,$TOKEN;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, "https://$SERVER/rest/$JID/".$path);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($curl, CURLOPT_USERPWD, $LOGIN . ":" . $TOKEN);

  $return = curl_exec($curl);
  curl_close($curl);
  return $return;
}
function get_result($url) {
  global $SERVER,$JID,$LOGIN,$TOKEN;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($curl, CURLOPT_USERPWD, $LOGIN . ":" . $TOKEN);

  $return = curl_exec($curl);
  curl_close($curl);
  return $return;
}
# Creates a job and get its id
$cfile = curl_file_create($FILE, 'text/plain', $FILE);
$parameter = array('config' => $cfile);


$xml = new SimpleXMLElement(post_uws($parameter));
$items = $xml->xpath('./uws:jobId');
$job_id = $items[0];

post_uws(array('PHASE'=>'RUN'), "$job_id/phase/");

print("$job_id\n");

while (true) {
  $xml = new SimpleXMLElement(get_uws("$job_id/"));
  $items = $xml->xpath('./uws:phase');
  $status = $items[0];
  if ($status == "COMPLETED") {
	  print("\nJob completed\n");
	  $xml = new SimpleXMLElement(get_uws("$job_id/results"));
	  $items = $xml->xpath('./uws:result');
	  foreach($items as $item) {
             $filename = $item->attributes()['id'];
	     $url = $item->attributes('http://www.w3.org/1999/xlink')[0];
	     print("get $filename from $url\n");
	     file_put_contents($filename, get_result($url));
	  }

	  break;
  }
  elseif ($status == "ERROR") {
	  print("\nJob error :\n");
	  print(get_uws("$job_id/stderr"));
	  print("\n");
	  break;
  }
  print(".");
  sleep(2);
}
exit(0);

?>
